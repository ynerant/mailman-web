=============
Mailman Web
=============

This is a Django project that contains default settings and url settings for
Mailman 3 Web Interface. It consists of the following sub-projects:

* Postorius
* Hyperkitty

Install
=======

To install this project, you run::

  $ pip install mailman-web

If you want to install the latest development version from Git, run::

  $ pip install git+https://gitlab.com/mailman/mailman-web


License
=======

Mailman suite is licensed under the
`GNU GPL v3.0 or later (GPLv3+) <http://www.gnu.org/licenses/gpl-3.0.html>`_

Copyright (C) 2020 by the Free Software Foundation, Inc.
